# React Interview

This project contains a simple React application to display a paginated table of users which can be filtered.

The required features are working and have already been implemented, but the implementation can be improved in many different ways.

Given the existing solution, refactor and tidy the code to what you believe would be a better implementation. The goal is to end up with a more flexible solution to be able to accommodate further required features.

To get started:
- clone
- `yarn install`
- `yarn start`

To run the tests:
- `yarn run test`

## TODO

1. Add language switch
2. Add language async downloading
3. Handle loading users data
4. (?) Move users loading responsibility to UsersTable
5. Add sane eslint config (airbnb?)
6. Remove css library
7. Add typescript
8. Add router
9. Make enzyme shallow testing work with hooks (not sure why adapter does not work)
10. Improve error handling (usePromise)