import React from 'react';
import { UsersTable } from './pages/UsersTable';
import './App.css';
import { usePromise } from './hooks/usePromise';
import { UsersApi } from './api/UsersApi';

const App = () => {
  const { value: users } = usePromise(() => UsersApi.getUsers(), [], []);

  return (
    <div className="p-app container">
      <UsersTable rows={users} />
    </div>
  );
}

export default App;
