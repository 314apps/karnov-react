import React from 'react';
import PropTypes from 'prop-types';

export const Page = ({ pageNumber, isActive, onChange }) => {
  const renderedPageNumber = pageNumber + 1;

  const handleClick = (event) => {
    event.preventDefault();
    onChange(pageNumber);
  }

  const itemClassNames = ['page-link'];

  if (isActive) {
    itemClassNames.push('button-outline');
  }

  return (
    <li className="c-pagination_item page-item">
      <button className={itemClassNames.join(' ')} onClick={handleClick}>{renderedPageNumber}</button>
    </li>
  );
}

Page.propTypes = {
  pageNumber: PropTypes.number.isRequired,
  isActive: PropTypes.bool.isRequired,
  onChange: PropTypes.func.isRequired,
}