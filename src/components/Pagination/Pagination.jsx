import React from 'react';
import PropTypes from 'prop-types';
import { times } from '../../utils/times';
import { Page } from './Page'
import './Pagination.css';

export const Pagination = ({ currentPage, totalPageCount, onChange }) => {
  if (totalPageCount === 0) {
    return null;
  }

  const pages = times(totalPageCount, pageNumber => (
    <Page
      key={pageNumber}
      isActive={currentPage === pageNumber}
      pageNumber={pageNumber}
      onChange={onChange}
    />
  ));

  return (
    <ul className="c-pagination">
      {pages}
    </ul>
  );
};

Pagination.propTypes = {
  currentPage: PropTypes.number.isRequired,
  totalPageCount: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
}