import React from 'react';
import { mount } from 'enzyme';
import { Pagination } from './Pagination';

it('renders given number of pages', () => {
  const onChange = () => {};
  const wrapper = mount(<Pagination currentPage={0} totalPageCount={3} onChange={onChange} />);

  expect(wrapper.find('.c-pagination_item').length).toBe(3);
});

it('properly marks active page', () => {
  const onChange = () => {};
  const wrapper = mount(<Pagination currentPage={1} totalPageCount={3} onChange={onChange} />);

  expect(wrapper.find('.c-pagination_item').slice(1, 2).find('.button-outline').length).toBe(1);
});

it('renders nothing if no pages available', () => {
  const onChange = () => {};
  const wrapper = mount(<Pagination currentPage={1} totalPageCount={0} onChange={onChange} />);

  expect(wrapper.find('.c-pagination').length).toBe(0);
});
