import { useState, useMemo } from 'react';

/**
 * Frontend pagination to be used with Pagination component
 */
export const usePagination = (perPage, data) => {
  const [page, setPage] = useState(0);

  const slicedData = useMemo(() => {
    const start = page * perPage;
    const end = start + perPage;

    return data.slice(start, end)
  }, [data, page, perPage]);

  return {
    page,
    perPage,
    setPage,
    totalPageCount: Math.ceil(data.length / perPage),
    data: slicedData,
  };
};