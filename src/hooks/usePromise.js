import { useEffect, useState } from 'react';

/**
 * Resolve promise with value (works on mounted components).
 * @note does not stops previous promise if deps are changed.
 * 
 * @param {Promise} promise
 */
export const usePromise = (makePromise, defaultValue, deps) => {
  const [value, setValue] = useState(defaultValue);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    let isMounted = true;

    makePromise()
      .then(value => {
        if (isMounted) {
          setValue(value);
        }
      })
      .finally(() => {
        if (isMounted) {
          setIsLoading(false);
        }
      });

    return () => { isMounted = false };
  }, deps);

  return { value, isLoading };
}

/* eslint react-hooks/exhaustive-deps: 0 */
