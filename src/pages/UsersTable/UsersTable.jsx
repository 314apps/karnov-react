import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Pagination } from '../../components';
import { Row } from './components/Row'
import { Search } from './components/Search'
import { usePagination } from '../../hooks/usePagination'
import { useFilterUsers } from './useFilterUsers';
import './UsersTable.css';

export const UsersTable = ({ rows }) => {
  const [filter, setFilter] = useState('');
  const filteredData = useFilterUsers(rows, filter);
  const { data: paginatedData, ...pagination } = usePagination(5, filteredData);

  const handleFilterChange = (newFilter) => {
    setFilter(newFilter);
    pagination.setPage(0);
  };

  const rowsToRender = paginatedData.map(row => <Row key={row.per_id} row={row} />);

  return (
    <div className='p-users-table'>
      <Search onSearch={handleFilterChange} />
      <table className='p-users-table_table'>
        <tbody>
          { rowsToRender }
        </tbody>
      </table>
      <Pagination
        currentPage={pagination.page}
        totalPageCount={pagination.totalPageCount}
        onChange={pagination.setPage} />
    </div>
  );
}

UsersTable.propTypes = {
  rows: PropTypes.arrayOf(PropTypes.object).isRequired,
}