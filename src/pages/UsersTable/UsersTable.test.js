import React from 'react';
import { mount } from 'enzyme';
import { UsersTable } from './UsersTable';

const rows = [
  {
    name1: 'Mads L. Klausen',
    email: 'MadsLKlausen@jourrapide.com',
    edit_path: 'http://google.com',
    per_id: 1
  },
  {
    name1: 'Alfred K. Krogh',
    email: 'AlfredKKrogh@armyspy.com',
    edit_path: 'http://google.com',
    per_id: 2
  },
  {
    name1: 'Silas L. Bertelsen',
    email: 'SilasLBertelsen@armyspy.com',
    edit_path: 'http://google.com',
    per_id: 3
  },
  {
    name1: 'Mia A. Johnsen',
    email: 'MiaAJohnsen@dayrep.com',
    edit_path: 'http://google.com',
    per_id: 4
  },
  {
    name1: 'Alfred S. Schou',
    email: 'AlfredSSchou@jourrapide.com',
    edit_path: 'http://google.com',
    per_id: 5
  }
];

it('renders 5 rows', () => {
  const wrapper = mount(<UsersTable rows={rows}/>);

  expect(wrapper.find('.p-users-table_row').length).toBe(5);
});

it('filters rows based on input', () => {
  const wrapper = mount(<UsersTable rows={rows} />);

  wrapper.find('input').simulate('change', { target: { value: 'k' } });

  expect(wrapper.find('.p-users-table_row').length).toBe(2);
});
