import React from 'react';
import PropTypes from 'prop-types';

export const Row = ({ row }) => {
  return (
    <tr className='p-users-table_row'>
      <td className='p-users-table_cell'>
        <a href={row.edit_path} className='p-users-table_user-name'>
          {row.name1}
        </a>
        <span className='p-users-table_user-email'>{row.email}</span>
      </td>
    </tr>
  );
};

Row.propTypes = {
  row: PropTypes.object.isRequired,
}