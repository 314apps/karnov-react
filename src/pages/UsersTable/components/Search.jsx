import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

export const Search = ({ onSearch }) => {
  const { t } = useTranslation();

  const handleSearch = (event) => onSearch(event.target.value);

  return (
    <div className="p-users-table_search">
      <input
        type="search"
        className="form-control"
        placeholder={t('UsersTable.search')}
        onChange={handleSearch} />
    </div>
  );
};

Search.propTypes = {
  onSearch: PropTypes.func.isRequired,
}