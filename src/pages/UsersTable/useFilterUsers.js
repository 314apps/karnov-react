import { useMemo } from 'react';

/**
 * Domain specific user filtering algorithm
 */
export const useFilterUsers = (users, originalFilter) => {
  const filter = originalFilter.toLowerCase();

  const value = useMemo(() => {
    if (!filter) {
      return users;
    }

    return users.filter((user) => {
      const hasMatchingName = user.name1.toLowerCase().indexOf(filter) > -1;
      const hasMatchingEmail = user.email && user.email.toLowerCase().indexOf(filter) > -1;

      return hasMatchingName || hasMatchingEmail;
    });
  }, [users, filter]);

  return value;
}
