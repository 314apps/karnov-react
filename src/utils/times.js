/**
 * Callbacks `cb` with 0..n values.
 *
 * @param {number} n iterations
 * @param {*} cb 
 */
export const times = (n, cb) => new Array(n).fill(null).map((v, k) => cb(k));